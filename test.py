#!/usr/bin/env python

import urllib2, json
import sys; sys.path.append("./lib/spotify-websocket-api")
from spotify_web.spotify import SpotifyAPI, SpotifyUtil

limitArg = "--limit="

if len(sys.argv) < 3:
	print "Usage: " + sys.argv[0] + " <username> <password> [" + limitArg + "100]"
	sys.exit(1)

user = sys.argv[1]
pwd = sys.argv[2]
limit = 100
if len(sys.argv) > 3:
	limitArgValue = sys.argv[3]
	if limitArgValue.startswith(limitArg):
		limit = int(limitArgValue[len(limitArg):])
		print "Limit is " + str(limit)
	else:
		print "Limit default value is 100"
else:
		print "Limit default value is 100"

sp = SpotifyAPI()
if not sp.connect(user, pwd):
	sys.exit()

url = "http://localhost:8080/spotify/playlistCollection"

def createArtist(artist):
	a = {}
	a['uri'] = SpotifyUtil.gid2uri("artist", artist.gid)
	a['name'] = artist.name
	return a

def createAlbum(album):
	a = {}
	a['uri'] = SpotifyUtil.gid2uri("album", album.gid)
	a['name'] = album.name
	return a

def createTrack(track):
	#print track.__str__()
	t = {}
	t['uri'] = SpotifyUtil.gid2uri("track", track.gid)
	t['name'] = track.name
	t['artists'] = []
	for artist in track.artist:
		t['artists'].append(createArtist(artist))
	t['album'] = createAlbum(track.album)
	t['duration'] = track.duration
	return t

playlists = []

def regPlaylist(playlist_uri, playlist):
	#print playlist.__str__()
	# FIXME virer le hasattr ?
	if hasattr(playlist, 'attributes'):
		p = {}
		p['uri'] = playlist_uri
		p['name'] = playlist.attributes.name
		p['tracks'] = []
		if playlist.length > 0:
			track_uris = [track.uri for track in playlist.contents.items if not SpotifyUtil.is_local(track.uri)]
			tracks = sp.metadata_request(track_uris)
			if hasattr(tracks, '__iter__'):
				for track in tracks:
					p['tracks'].append(createTrack(track))
			else:
				p['tracks'].append(createTrack(tracks))
		playlists.append(p)

count = 0

def requestPlaylists(begin, interval):
	global count
	print str(begin) + " - " + str(begin + interval)
	playlist_uris = [playlist.uri for playlist in sp.playlists_request(user, begin, interval).contents.items]
	for playlist_uri in playlist_uris:
		count += 1
		playlist = sp.playlist_request(playlist_uri)
		regPlaylist(playlist_uri, playlist)

begin = 0
leftLimit = limit
# TODO check si user a 100 playlists tout rond
# meme pb s'il en a 0 ?
while leftLimit > 0:
	if(count < begin):
		print "stop before limit"
		break
	interval = min(100, leftLimit)
	requestPlaylists(begin, interval)
	begin += 100
	leftLimit -= 100

print str(count) + " playlists"

jsonRequest = json.dumps(playlists)
#print jsonRequest

headers = {}
headers['Content-Type'] = 'application/json'
req = urllib2.Request(url, jsonRequest, headers)
urllib2.urlopen(req)
